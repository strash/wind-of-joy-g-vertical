extends Control


signal level1_pressed
signal level2_pressed
signal level3_pressed


var grid_size: Vector2
var grid_pos: float
var tween_speed: float = 0.15
var input_relative: float = 0.0


# BUILTINS - - - - - - - - -


func _ready() -> void:
	grid_size = ($Grid as VBoxContainer).rect_size
	grid_pos = ($Grid as VBoxContainer).rect_position.y


func _input(event: InputEvent) -> void:
	if grid_size.y > get_viewport_rect().size.y:
		# вертикальный скроллинг плиток
		if event is InputEventScreenDrag:
			($Grid as VBoxContainer).rect_position.y += event.relative.y
			input_relative = event.relative.y
		# когда перестали скролить проверяем позицию плиток
		elif event is InputEventScreenTouch and not event.is_pressed():
			set_page()


# METHODS - - - - - - - - -


# выравнивание плиток, если они далеко проскролены
func set_page() -> void:
	var view_h: float = get_viewport_rect().size.y
	var grid: VBoxContainer = $Grid as VBoxContainer
	var _t: int
	if grid.rect_position.y > grid_pos:
		_t = ($Tween as Tween).interpolate_property(grid, "rect_position:y", null, grid_pos, tween_speed)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(tween_speed + 0.1), "timeout")
		input_relative = 0.0
	elif grid.rect_position.y + grid.rect_size.y < view_h - 100.0:
		_t = ($Tween as Tween).interpolate_property(grid, "rect_position:y", null, view_h - 100.0 - grid.rect_size.y, tween_speed)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(tween_speed + 0.1), "timeout")
		input_relative = 0.0


# SIGNALS - - - - - - - - -


func _on_BtnLvl1_pressed() -> void:
	if input_relative == 0.0:
		emit_signal("level1_pressed")


func _on_BtnLvl2_pressed() -> void:
	if input_relative == 0.0:
		emit_signal("level2_pressed")


func _on_BtnLvl3_pressed() -> void:
	if input_relative == 0.0:
		emit_signal("level3_pressed")

